import React, { useState, useEffect } from "react";

import firebase from "../firebase";

const TimesList = () => {
  const SORT_OPTIONS = {
    TIME_ASC: { column: "time_seconds", direction: "asc" },
    TIME_DESC: { column: "time_seconds", direction: "desc" },

    TITLE_ASC: { column: "title", direction: "asc" },
    TITLE_DESC: { column: "title", direction: "desc" },
  };

  function useTimes(sortBy = "TIME_ASC") {
    const [times, setTimes] = useState([]);

    useEffect(() => {
      const unsubscrible = firebase
        .firestore()
        .collection("times")
        .orderBy(SORT_OPTIONS[sortBy].column, SORT_OPTIONS[sortBy].direction)
        .onSnapshot((snapshot) => {
          const newTimes = snapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
          }));
          setTimes(newTimes);
        });
      return () => unsubscrible();
    }, [sortBy]);
    return times;
  }
  const [sortBy, setSortBy] = useState("TIME_ASC");
  const times = useTimes(sortBy);

  let lists = times.map((time) => (
    <li key={time.id}>
      <div className="time-entry">
        {time.title}
        <code>{time.time_seconds} seconds</code>
      </div>
    </li>
  ));

  return (
    <div>
      <h2>Times List</h2>
      <div>
        <label>Sort By:</label>
        <select
          value={sortBy}
          onChange={(e) => {
            setSortBy(e.currentTarget.value);
          }}
        >
          <option disabled>---</option>
          <option value="TIME_ASC">Time (fastest first)</option>
          <option value="TIME_DESC">Time (slowest first)</option>
          <option value="TITLE_ASC">Title (A - Z)</option>
          <option value="TITLE_DESC">Title (Z - A)</option>
        </select>
      </div>
      <ol>{lists}</ol>
    </div>
  );
};

export default TimesList;
